class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title, :message => "Cannot be empty"
	validates_presence_of :body, :message => "Cannot be empty"
	validates :title, length: {minimum: 3}
end
